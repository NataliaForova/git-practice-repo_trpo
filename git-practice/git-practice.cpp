// git-practice.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

static const char *INPUT_FILE = "input.txt";
static const char *OUTPUT_FILE = "output.txt";

template <typename T>
struct functor
{
    void operator()(T& e)
    {
        // not implemented
    }
};

template <typename T>
struct predicate
{
    bool operator()(T e)
    {
        // not implemented
        return true;
    }
};

template <typename T>
struct printer
{
    void operator()(T& e)
    {
        // not implemented
    }
};

template <typename T>
class input_reader 
{
public:
    std::vector<T> operator()(const char* filename)
    {
        std::vector<T> out;

        // not implemented

        return out;
    }
};

template <typename T>
class output_writer
{
public:
    void operator()(const char* filename, std::vector<T>& vec)
    {
        // not implemented
    }
};

int _tmain(int argc, _TCHAR* argv[])
{
    std::vector<int> source_v;

    using curr_type = std::remove_reference<decltype(source_v[0])>::type;

    std::vector<curr_type> target_v;

    input_reader<curr_type> read;
    source_v = read(INPUT_FILE);

    predicate<curr_type> pred;
    std::copy_if(source_v.begin(), source_v.end(), std::back_inserter(target_v), pred);

    functor<curr_type> func;
    std::for_each(target_v.begin(), target_v.end(), func);

    printer<curr_type> printr;
    std::for_each(target_v.begin(), target_v.end(), printr);

    output_writer<curr_type> write;
    write(OUTPUT_FILE, target_v);

    return 0;
}
